//
//  AppDelegate.swift
//  Test Task
//
//  Created by Kseniya on 7/11/20.
//  Copyright © 2020 kseniya. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    static var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.regularLaunching()
        self.setupRealm()
        return true
    }
    
    private func regularLaunching() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let listVC = ListVC()
            let navigationController = BaseNavigationVC(rootViewController: listVC)
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
        }
    }
    
    func setupRealm() {
        var realmConfiguration = Realm.Configuration.defaultConfiguration
        realmConfiguration.deleteRealmIfMigrationNeeded = true
        Realm.Configuration.defaultConfiguration = realmConfiguration
    }
    
}

