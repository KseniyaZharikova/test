//
//  WeatherService.swift
//  Test Task
//
//  Created by Kseniya on 7/11/20.
//  Copyright © 2020 kseniya. All rights reserved.
//

import Foundation
import Moya

protocol UsersNetworkable {
    func loadWeather(_ completion: @escaping (Result<ForecastModel, Error>) -> Void)
    
}

struct WeatherService: UsersNetworkable {
    
    private let provider = NetworkProvider<WeatherTarget>()
    
    func loadWeather( _ completion: @escaping (Result<ForecastModel, Error>) -> Void) {
        provider.request(.loadWeather, onSuccess: { (response) in
            do {
                print(try response.mapJSON())
                let baseResp = try response.map(ForecastModel.self)
                completion(.success(baseResp))
            } catch {
                do {
                    let baseResp = try response.map(BaseResponse.self)
                    completion(.failure(NSError(domain:"", code:0, userInfo:[ NSLocalizedDescriptionKey: baseResp.message ??  ""])))
                } catch {
                    completion(.failure(error))
                }
            }
        }) { (error) in
            completion(.failure(error))
        }
    }
}

enum WeatherTarget {
    case loadWeather
}

extension WeatherTarget: TargetType {
    var headers: [String : String]? {return [:]}
    var path: String {
        switch self {
        case .loadWeather:
            return "forecast/daily"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .loadWeather:
            return .get
        }
    }
    
    var task: Task {
        switch self {
        case .loadWeather:
            return .requestParameters(parameters: requestParameters, encoding: URLEncoding.default)
        }
    }
    
    var requestParameters: [String : Any] {
        switch self {
        case .loadWeather:
            return ["id" : "5091","appid":"8a8b518a408d1cd5884575df0582255b"]
        }
    }
}
