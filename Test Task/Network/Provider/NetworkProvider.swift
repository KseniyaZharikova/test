//
//  NetworkProvider.swift
//  Test Task
//
//  Created by Kseniya on 7/11/20.
//  Copyright © 2020 kseniya. All rights reserved.
//


import Foundation
import Moya
import Alamofire

class NetworkProvider<Target: TargetType> {
    
    private let provider: MoyaProvider<Target>
    let UNAUTHORIZED_CODE = 401
    let UNACTUAL_CODE = 426
    let SUCCESS_CODE = 200
    
    init() {
        #if DEBUG
                  provider = MoyaProvider(plugins: [NetworkLoggingPlugin()])
              #else
                  provider = MoyaProvider()
              #endif
    }
    
    func request(_ target: Target,
                 onSuccess: @escaping (Response) -> Void,
                 onFailure: @escaping (Error) -> Void) {
        
        if NetworkReachabilityManager()?.isReachable == false {
            DispatchQueue.main.async {
                print("No Internet")
            }
            return
        }
        
        provider.request(target) { result in
            switch result {
            case .success(let response):
                do {
                    let filteredResponse = try response.filterSuccessfulStatusCodes()
                    onSuccess(filteredResponse)
                }
                catch let error {
                    if let baseMessageResponse = try? response.map(BaseResponse.self), baseMessageResponse.message != nil {
                        onSuccess(response)
                    } else {
                        onFailure(error)
                    }
                }
            case .failure(let error):
                onFailure(error)
            }
        }
        
        
    }
}

extension TargetType {
    
    var sampleData: Data { Data() }
    
    var baseURL: URL {
        let stringURL = "https://samples.openweathermap.org/data/2.5/"
        return URL(string: stringURL)!
    }
}
