//
//  DetailsVC.swift
//  Test Task
//
//  Created by Kseniya on 7/11/20.
//  Copyright © 2020 kseniya. All rights reserved.
//

import Foundation
import UIKit

class DetailsVC: BaseVC {
    
    var item = ItemModel()
    
    private let degLabel: UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        return label
    }()
    
    private let decriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        return label
    }()
    
    private let speedLabel: UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        return label
    }()
    
    private let pressureLabel: UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        return label
    }()
    
    private let humidityLabel: UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [decriptionLabel,degLabel,speedLabel, pressureLabel,humidityLabel])
        stackView.axis = .vertical
        stackView.spacing = 10
        return stackView
    }()
    
    static func newInstance(item: ItemModel) -> DetailsVC {
        let vc = DetailsVC()
        vc.item = item
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupConstraint()
        fillData()
    }
    
    override func setupConstraint() {
        super.setupConstraint()
        
        view.addSubviews([stackView])
        
        stackView.snp.makeConstraints {
            $0.top.equalTo(self.view.safeAreaLayoutGuide.snp.topMargin).offset(20)
            $0.left.equalTo(20)
        }
        
    }
    
    func fillData() {
        
        navigationItem.title = item.date.dateString()
        decriptionLabel.text = "\(item.weather[0].descript)"
        degLabel.text = "Celsius: \(item.deg)"
        speedLabel.text = "Wind: \(item.speed)"
        pressureLabel.text = "Pressure: \(item.pressure)"
        humidityLabel.text = "Humidity: \(item.humidity)"
    }
}
