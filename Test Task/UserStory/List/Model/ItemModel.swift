//
//  ItemModel.swift
//  Test Task
//
//  Created by Kseniya on 7/11/20.
//  Copyright © 2020 kseniya. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

@objcMembers class ItemModel: Object,Decodable {
    
    dynamic var date: Int = 0
    dynamic var pressure: Float = 0.0
    dynamic var speed: Float = 0.0
    dynamic var deg: Double = 0.0
    dynamic var humidity: Int = 0
    let weather = List<WeatherModel>()
    
    required  init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        date = (try? container.decode(Int.self, forKey: .date)) ?? 0
        pressure = (try? container.decode(Float.self, forKey: .date)) ?? 0.0
        speed = (try? container.decode(Float.self, forKey: .speed)) ?? 0.0
        deg = (try? container.decode(Double.self, forKey: .deg)) ?? 0.0
        humidity = (try? container.decode(Int.self, forKey: .humidity)) ?? 0
        
        let item = try container.decode([WeatherModel].self, forKey: .weather)
        weather.append(objectsIn: item)
        super.init()
    }

    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema){
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    enum Property: String {
        case id
    }
}

private extension ItemModel {
    enum CodingKeys: String, CodingKey {
        case weather ,date = "dt",pressure,humidity,speed,deg
    }
}
