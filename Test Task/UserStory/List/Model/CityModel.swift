//
//  CityModel.swift
//  Test Task
//
//  Created by Kseniya on 7/11/20.
//  Copyright © 2020 kseniya. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

@objcMembers class CityModel: Object, Decodable {
    
    // constant id for only update (api does not specific id )
    dynamic var id = "122"
    
    var name:String = ""
   
    required  init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = (try? container.decode(String.self, forKey: .name)) ?? ""
        super.init()
    }
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema){
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    override static func primaryKey() -> String? {
        return CityModel.Property.id.rawValue
    }
    
    enum Property: String {
        case id
    }
    
}

private extension CityModel {
    enum CodingKeys: String, CodingKey {
        case name
    }
}


