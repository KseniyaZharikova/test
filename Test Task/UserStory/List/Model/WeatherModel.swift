//
//  WeatherModel.swift
//  Test Task
//
//  Created by Kseniya on 7/11/20.
//  Copyright © 2020 kseniya. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

@objcMembers class WeatherModel : Object, Decodable  {
    
    dynamic var descript: String = ""
    
    required  init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        descript = (try? container.decode(String.self, forKey: .description)) ?? ""
        super.init()
    }
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema){
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    enum Property: String {
        case id
    }
}

private  extension WeatherModel {
    enum CodingKeys: String, CodingKey {
        case description
    }
}
