//
//  ListCell.swift
//  Test Task
//
//  Created by Kseniya on 7/11/20.
//  Copyright © 2020 kseniya. All rights reserved.
//

import Foundation
import UIKit

class ListCell: BaseTVCell {
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .gray
        return label
    }()
    
    override func setupUI() {
        selectionStyle = .none
    }
    
    override func  addSubViews() {
        addSubview(titleLabel)
        
        titleLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalTo(20)
            make.right.equalTo(0)
        }
    }
    
    func fillCell(_ date: Int){
        titleLabel.text = date.dateString()
    }
}

