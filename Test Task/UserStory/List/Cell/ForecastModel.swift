//
//  ForecastModel.swift
//  Test Task
//
//  Created by Kseniya on 7/11/20.
//  Copyright © 2020 kseniya. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

@objcMembers class ForecastModel:Object,Decodable {
    
    // constant id for only update (api does not specific id )
    
    dynamic var id = "123"
    let list = List<ItemModel>()
    dynamic var city: CityModel? = CityModel()
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let item = try container.decode([ItemModel].self, forKey: .list)
        list.append(objectsIn: item)
        city =  (try? container.decode(CityModel.self, forKey: .city)) ?? nil
        super.init()
    }
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema){
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    override static func primaryKey() -> String? {
        return ForecastModel.Property.id.rawValue
    }
    
    enum Property: String {
        case id
    }
}

private extension ForecastModel {
    
    enum CodingKeys: String, CodingKey {
        case list, city
    }
}
