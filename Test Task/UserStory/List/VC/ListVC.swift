//
//  ListVC.swift
//  Test Task
//
//  Created by Kseniya on 7/11/20.
//  Copyright © 2020 kseniya. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import RealmSwift

class ListVC: BaseVC {
    
    var viewModel = ListVM()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .singleLine
        tableView.register(cellWithClass: ListCell.self)
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.bind(self)
        viewModel.loadForecast()
    }
    
    override func setupConstraint() {
        super.setupConstraint()
        view.addSubview(tableView)
        
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}

extension ListVC: ListVMDelegate {
    
    func reloadTable() {
        self.tableView.reloadData()
    }
    
    func showErrorMessage(_ message: String) {}
    
}

extension ListVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  viewModel.getData()?.list.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UILabel()
        header.textColor = .gray
        header.backgroundColor = .clear
        header.textAlignment = .center
        header.text = "Daily Forecast"
        return header
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        navigationItem.title =  viewModel.getData()?.city?.name
        let cell = tableView.dequeueReusableCell(withClass: ListCell.self, for: indexPath)
       
        cell.fillCell( viewModel.getData()?.list[indexPath.row].date ?? 0)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailsVC = DetailsVC.newInstance(item:viewModel.getData()?.list[indexPath.row] ?? ItemModel())
        self.navigationController?.pushViewController(detailsVC, animated: true)
    }
}

