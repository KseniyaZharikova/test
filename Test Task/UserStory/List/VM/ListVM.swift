//
//  ListVM.swift
//  Test Task
//
//  Created by Kseniya on 7/11/20.
//  Copyright © 2020 kseniya. All rights reserved.
//

import Foundation
import RealmSwift

class ListVM {
    
    private let weatherService = WeatherService()
    weak var delegate: ListVMDelegate?
    var forecastModel = ForecastModel()
    let realm = try! Realm()
    
    func bind(_ delegate: ListVMDelegate) {
        self.delegate = delegate
    }
    
    func loadForecast() {
        weatherService.loadWeather( { [weak self] result in
            guard let `self` = self else { return }
            switch result {
            case .success(let response):
                self.saveData(forecastModel: response)
            case .failure(let error): self.delegate?.showErrorMessage(error.localizedDescription)
            }
        })
    }
    
    func saveData(forecastModel :ForecastModel ) {
        do {
            try realm.write {
                self.realm.add(forecastModel, update: Realm.UpdatePolicy.modified)
            }
        } catch {
            print(error.localizedDescription)
        }
        
        self.delegate?.reloadTable()
    }
    
    func getData() -> ForecastModel? {
        let realm = try! Realm()
        let forecast   =  realm.object(ofType: ForecastModel.self, forPrimaryKey: "123")
        return forecast
    }
}

protocol ListVMDelegate: BaseDelegate {
    func reloadTable()
}
