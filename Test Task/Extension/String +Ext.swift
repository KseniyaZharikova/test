
//
//  String +Ext.swift
//  Test Task
//
//  Created by Kseniya on 7/11/20.
//  Copyright © 2020 kseniya. All rights reserved.
//

import Foundation

extension String {
    
    static var empty: String { return "" }
    
    static var newLine: String { return "\n" }
    
    static func whitespace(count: Int = 1) -> String {
        return String(repeating: " ", count: count)
    }
    
}
