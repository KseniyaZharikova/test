//
//  Int +Ext.swift
//  Test Task
//
//  Created by Kseniya on 7/11/20.
//  Copyright © 2020 kseniya. All rights reserved.
//

import Foundation
extension Int {
    
    func dateString() -> String? {
        let dateFromTimestamp = Date(timeIntervalSince1970: TimeInterval(self))
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full
        var calendar = NSCalendar.current
        calendar.locale =  Locale(identifier: "en_US")
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, yyyy"
        dateFormatter.locale = Locale(identifier: "en_US")
        return String(format: dateFormatter.string(from: dateFromTimestamp))
        
    }
    
}
