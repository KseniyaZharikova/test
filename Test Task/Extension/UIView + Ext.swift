//
//  UIView + Ext.swift
//  Test Task
//
//  Created by Kseniya on 7/11/20.
//  Copyright © 2020 kseniya. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func addSubviews(_ subviews: [UIView]) {
        subviews.forEach { addSubview($0) }
    }
}
