//
//  BaseResponse.swift
//  Test Task
//
//  Created by Kseniya on 7/11/20.
//  Copyright © 2020 kseniya. All rights reserved.
//

import Foundation


class BaseResponse: Decodable {
    
    let message: String?
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        message = try container.decodeIfPresent(String.self, forKey: .message)
    }
    
    init() { message = nil }
}

private extension BaseResponse {
    
    enum CodingKeys: String, CodingKey {
        case message
    }
}
