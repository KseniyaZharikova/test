//
//  BaseDelegate.swift
//  Test Task
//
//  Created by Kseniya on 7/11/20.
//  Copyright © 2020 kseniya. All rights reserved.
//

import Foundation

protocol BaseDelegate: class {
    func showErrorMessage(_ message: String)
}
